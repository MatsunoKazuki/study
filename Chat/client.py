#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import threading
 
 
HOST = '127.0.0.1'
PORT = 9998

def input_name():
    return input('input name >>>')

def input_msg_roop(name, sock):
    """メッセージ入力を促し、サーバに送信する"""
 
    while True:
        msg = input('>>>')
        if msg == 'exit':
            break
        elif msg:
            
            sock.send((name + ":" + msg).encode('utf-8'))
 
 
def client_start(name):
    """クライアントのスタート"""
 
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((HOST, PORT))
    handle_thread = threading.Thread(target=handler, args=(sock,), daemon=True)
    handle_thread.start()
    try:
        input_msg_roop(name, sock)
    finally:
        sock.close()
 
 
def handler(sock):
    """サーバからメッセージを受信し、表示する"""
 
    while True:
        data = sock.recv(1024)
        print(data.decode("utf-8"))
 
 
if __name__ == "__main__":
    name = input_name()

    client_start(name)