#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import threading
 
 
HOST = ''
PORT = 9998
clients = []
 
 
def remove_conection(con, address):
    """クライアントと接続を切る"""
 
    print('[切断]{}'.format(address))
    con.close()
    clients.remove((con, address))
 
 
def server_start():
    """サーバをスタートする"""
 
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((HOST, PORT))
    sock.listen(10)
 
    while True:
        con, address = sock.accept()
        print("[接続]{}".format(address))
        clients.append((con, address))
        handle_thread = threading.Thread(target=handler,
                                         args=(con, address),
                                         daemon=True)
        handle_thread.start()
 
 
def handler(con, address):
    """クライアントからデータを受信する"""
 
    while True:
        try:
            data = con.recv(1024)
        except ConnectionResetError:
            remove_conection(con, address)
            break
        else:
            if not data:
                remove_conection(con, address)
                break
            else:
                print("[受信]{} - {}".format(address, data.decode("utf-8")))
                for c in clients:
                    c[0].sendto(data, c[1])
 
 
if __name__ == "__main__":
    server_start()
 