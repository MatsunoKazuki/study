# 問題
# 次のプログラムを実行したとき画面には何が表示されるか答えよ

nenrei = 5
kakaku = 0
if nenrei == 0:
    kakaku = 100
    print("年齢が0歳のとき、価格は", kakaku, "円です")
elif nenrei == 1:
    kakaku = 300
    print("年齢が1歳のとき、価格は", kakaku, "円です")
elif nenrei == 2:
    kakaku = 300
    print("年齢が2歳のとき、価格は", kakaku, "円です")
else:
    kakaku = 500
    print("年齢が0～2歳以外のとき、価格は", kakaku, "円です")
