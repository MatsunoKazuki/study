# ランダムに数字, 等符号, 数字を画面に出す
# True or Falseを入力する(大文字小文字も意識する）
# 答えを出す
# n回繰り返してクリア秒数、正解数を出す

import random
from enum import Enum
import time

class Mark(Enum):
    lt = 0  # <
    gt = 1  # >
    le = 2  # <=
    ge = 3  # >=
    eq = 4  # ==
    ne = 5  # !=

def mark_get(mark_enum):
    if mark_enum == Mark.lt:
        mark = '<'
    elif mark_enum == Mark.gt:
        mark = '>'
    elif mark_enum == Mark.le:
        mark = '<='
    elif mark_enum == Mark.ge:
        mark = '>='
    elif mark_enum == Mark.eq:
        mark = '=='
    elif mark_enum == Mark.ne:
        mark = '!='
    return mark


LOOP_COUNTER = 15
# 全体の開始時間
start_time = time.time()
# ミスった回数
miss_count = 0

for i in range(LOOP_COUNTER):
    num_left    = random.randint(0, 10) # 0~10までの数字を入れる
    num_right   = random.randint(0, 10) # 0~10までの数字を入れる

    mark_enum   = Mark(random.randint(0, 5))    # Enumの数字を入れてランダムにマークを取り出す
    mark        = mark_get(mark_enum)

    print(num_left, mark, num_right)
    # True or Flaseが入力されるまで無限ループ
    while(True):
        answer_a = input('True or False: ')
        if answer_a in {'True', 'False'}:
            break
    # 文字列をboolとして入れなおす
    if answer_a == 'True':
        answer_a = True
    else:
        answer_a = False

    if mark_enum == Mark.lt:
        answer_b = num_left < num_right
    elif mark_enum == Mark.gt:
        answer_b = num_left > num_right
    elif mark_enum == Mark.le:
        answer_b = num_left <= num_right
    elif mark_enum == Mark.ge:
        answer_b = num_left >= num_right
    elif mark_enum == Mark.eq:
        answer_b = num_left == num_right
    elif mark_enum == Mark.ne:
        answer_b = num_left != num_right
    
    if(answer_a == answer_b):
        print('正解！')
    else:
        print('違うよ')
        miss_count = miss_count + 1

    # 改行
    print()

end_time = time.time() - start_time
print('{0}個中、ミスは{1}個で{2}[sec]かかった'.format(LOOP_COUNTER, miss_count, round(end_time, 3)))

