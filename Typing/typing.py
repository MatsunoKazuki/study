import random
import time

try:
    # getchがインポート出来ればインポート
    from msvcrt import getch
except ImportError:
    # getchがインポートできなければ自身で作成
    def getch():
        import sys
        import tty
        import termios
        fd = sys.stdin.fileno()
        old = termios.tcgetattr(fd)
        try:
            tty.setraw(fd)
            return sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old)

# とりあえずホームポジションだけ
OUTPUT_LIST = ['a', 's', 'd', 'f', 'j', 'k', 'l', ';', 'g', 'h', ]
CTRL_C = 3
LOOP_COUNTER = 10


# 全体の開始時間
start_time = time.time()

# ミスった回数
miss_count = 0

# for文で0~COUNTER(現状だと10)まで回す
for i in range(LOOP_COUNTER):
    output = random.choice(OUTPUT_LIST)
    start_input_time = time.time()
    while True:
        print('これを入力だ！-> ' + output)
        key = ord(getch())
        if key == CTRL_C:
            # 終わり
            sys.exit()
        elif chr(key) == output:
            end_input_time = time.time() - start_input_time
            message = '{0} : {1}[sec]'.format(chr(key), round(end_input_time, 3))
            print(message)
            break
        else:
            print('チガウヨ')
            miss_count += 1

end_time = time.time() - start_time
print('{0}個入力、ミスは{1}個で{2}[sec]かかった'.format(LOOP_COUNTER, miss_count, round(end_time, 3)))
